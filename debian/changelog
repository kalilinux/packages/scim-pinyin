scim-pinyin (0.5.91-2) unstable; urgency=low

  * Removed old transitional package: scim-chinese (Closes: #477536)
    and made related changes.
  * Updated copyright file to use proper GPL-2 instead of GPL.
  * Updated control file to update dependencies and VCS entries.
  * Updated Standards-Version to 3.9.1.
  * Fixed typo in 0.5.91-1.1 in changelog.
  * Did not add the skim-scim-pinyin package like Ubuntu due to Debian 
    freeze etc.  Updated README.Debian.

 -- Osamu Aoki <osamu@debian.org>  Sat, 11 Dec 2010 15:11:54 +0900

scim-pinyin (0.5.91-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Explicitly depend on libltdl-dev (Closes: #590412).

 -- Nico Golde <nion@debian.org>  Wed, 04 Aug 2010 13:43:00 +0200

scim-pinyin (0.5.91-1) unstable; urgency=low

  [ Deng Xiyue ]
  * New upstream release.  (Closes: #459869)
    + Fix FTBFS with gcc-4.3.  (Closes: #441594)
  * Drop 01gcc-4.0.dpatch.  Fixed upstream.
  * Drop -1 debian version from libscim-dev b-dep as per lintian.

  [ Anthony Fok ]
  * Merged 0.5.91-0ubuntu12 from Ubuntu.  Many thanks to Zhengpeng Hou
    and others for their great works!  Remaining differences:
    - Do not build skim-scim-pinyin for Debian (will do so in -2).
    - Keep Ming Hua's 10_missing-header.dpatch (#define Use_C_STRING)
      as per upstream practice rather than "#include <cstring>".
    - Do not install /etc/X11/xinit/xinput.d/scim-pinyin.  TBD.
  * debian/rules:
    - Use -O2 instead of -O
    - configure: --disable-skim-support for now.
    - build: $(MAKE) distclean DIST_SUBDIRS='m4 src data intl po' (no skim)
  * debian/copyright:
    - Copyright (C) 2002-2005 (was 2004).

 -- Anthony Fok <foka@debian.org>  Sun, 27 Jul 2008 12:09:37 +0800

scim-pinyin (0.5.0-4.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add '20_gcc_4.3.dpatch' to fix FTBFS with GCC 4.3 (Closes: #441594)

 -- Chris Lamb <chris@chris-lamb.co.uk>  Sat, 12 Apr 2008 02:07:04 +0100

scim-pinyin (0.5.0-4) unstable; urgency=low

  * Rewrite debian/rules, using debian/rules from scim-tables as template.
    - Use the platform detection code recommended by autotools-dev.
    - Remove binary-common target and separate binary-arch and binary-indep
      targets completely.
    - Remove all the cruft introduced by dh_make.
  * Introduce 10_missing-header dpatch, adding missing <cstring> header in a
    few files and fixing FTBFS with GCC 4.3.  (Closes: #441594)
  * Use "dpkg-query -W" in scim-chinese's preinst script to check conffiles
    instead of parsing /var/lib/dpkg/status directly.
  * Put upstream homepage into the Homepage field of the source package
    instead of the Description field of the binary package.
  * Rename XS-Vcs-* fields to Vcs-* as they are officially supported now.
  * Add "dpkg-dev (>= 1.14.6)" in build dependency, due to the above usage of
    Homepage and Vcs-* fields.
  * Upgrade policy version to 3.7.3.
  * Change maintainer's email address.

 -- Ming Hua <minghua-guest@users.alioth.debian.org>  Sat, 02 Feb 2008 12:05:33 -0600

scim-pinyin (0.5.91-0ubuntu12) gutsy; urgency=low

  * Fix build failure if the build target is run without running
    the clean target. LP: #143960.

 -- Matthias Klose <doko@ubuntu.com>  Thu, 11 Oct 2007 17:50:50 +0000

scim-pinyin (0.5.91-0ubuntu11) gutsy; urgency=low

  * Fix build failures with g++-4.3.

 -- Matthias Klose <doko@ubuntu.com>  Mon, 10 Sep 2007 16:16:53 +0200

scim-pinyin (0.5.91-0ubuntu10) gutsy; urgency=low

  * Add build dependency on skim. Fixes another build failure.
  * Correctly clean the package, run `make clean'.

 -- Matthias Klose <doko@ubuntu.com>  Thu, 30 Aug 2007 09:40:44 +0000

scim-pinyin (0.5.91-0ubuntu9) gutsy; urgency=low

  * Run clean target, fixes build failure. LP: #128220.

 -- Matthias Klose <doko@ubuntu.com>  Thu, 30 Aug 2007 09:15:35 +0000

scim-pinyin (0.5.0-3) unstable; urgency=low

  * Adapt to scim's $(moduledir) change:  (Closes: #422739)
    - Bump build dependency to libscim-dev (>= 1.4.6-1).
    - Drop 10scim-plugin-module-install-dir dpatch, no longer needed.
    - Use /usr/lib/scim-1.0/*/ instead of /usr/lib/scim-1.0/1.4.0/ in
      debian/rules and debian/scim-pinyin.install, so that scim-pinyin can be
      binNMUed for the next SCIM module ABI change.
  * Update debian/watch file to use qa.debian.org redirect URL for
    sourceforge.net.
  * Add XS-Vcs-Svn and XS-Vcs-Browser fields in debian/control.
  * Upgrade to policy version 3.7.2.2, no actual changes needed.

 -- Ming Hua <minghua@rice.edu>  Sun, 13 May 2007 17:54:00 -0500

scim-pinyin (0.5.91-0ubuntu8) gutsy; urgency=low

  * Drop 10scim-plugin-module-install-dir.dpatch, because scim 
    has changes, due to the upgrade of libgtk2.0.
  * Build-dep on libscim-dev (>= 1.4.6-1).
  * Use /usr/lib/scim/1.0/*/ to install modules.

 -- ZhengPeng Hou <zhengpeng-hou@ubuntu.com>  Fri, 11 May 2007 21:40:45 +0800

scim-pinyin (0.5.91-0ubuntu7) feisty; urgency=low

  * Rebuild for ldbl128 change (powerpc, sparc).
  * Set Ubuntu maintainer address.

 -- Matthias Klose <doko@ubuntu.com>  Mon,  5 Mar 2007 01:24:54 +0100

scim-pinyin (0.5.91-0ubuntu6) edgy; urgency=low

  * Correct the scim-bridge's dir in 
    debian/im-switch/scim-pinyin, due to the 
    change in gtk2

 -- Hou ZhengPeng <zhengpeng.hou@gmail.com>  Wed, 16 Aug 2006 08:24:12 +0800

scim-pinyin (0.5.91-0ubuntu5) edgy; urgency=low

  * Fix the FTBFS, due to the change in scim
    add build-dep on scim-gtk2-immodule

 -- Hou ZhengPeng <zhengpeng.hou@gmail.com>  Tue,  1 Aug 2006 22:29:05 +0800

scim-pinyin (0.5.91-0ubuntu4) edgy; urgency=low

  * Improve the conffile of im-switch in
    debian/im-switch/scim-pinyin

 -- Hou ZhengPeng <zhengpeng.hou@gmail.com>  Tue, 18 Jul 2006 14:04:24 +0800

scim-pinyin (0.5.91-0ubuntu3) dapper; urgency=low

  * check for skim in the im-switch file and sets XIM_PROGRAM=skim if 
    it is installed (fixes the problem that skim is not started currently 
    automatically in kubuntu)
  
 -- Hou ZhengPeng <zhengpeng.hou@gmail.com>  Thu, 23 Mar 2006 11:48:37 +0800

scim-pinyin (0.5.91-0ubuntu2) dapper; urgency=low

  * Add im-switch configure file in debian/im-switch for postinst.
  * Add im-switch to Recommends.
  * Use ttf-arphic-uming and ttf-arphic-ukai as recommend chinese fonts .
  * Fix bug #34413 on malone.
  * Upload for Hou ZhengPeng <zhengpeng.hou@gmail.com>.

 -- Jonathan Patrick Davies <jpatrick@ubuntu.com>  Sat,  11 Mar 2006 15:53:47 +0100

scim-pinyin (0.5.91-0ubuntu1) dapper; urgency=low

  * New upstream release 
  * Add skim-scim-pinyin and build-dep on skim
  * Add postinst for register scim to im-switch 

 -- Hou ZhengPeng <zhengpeng.hou@gmail.com>  Thu,  2 Mar 2006 19:18:00 +0800

scim-pinyin (0.5.0-2) unstable; urgency=low

  * Make scim-pinyin Replaces scim-chinese (<< 0.5.0) to provide smooth
    upgrade from sarge.  (Closes: #343562)
  * Handle obsolete conffile from old scim-chinese packages properly.
  * Build-depend on autotools-dev and use config.{guess,sub} from
    autotools-dev unconditionally.

 -- Ming Hua <minghua@rice.edu>  Sun, 25 Dec 2005 04:59:22 -0600

scim-pinyin (0.5.0-1) unstable; urgency=low

  * New upstream release.  [minghua]
    - Upstream changes name to scim-pinyin.
  * debian/patches/10scim-plugin-module-install-dir.dpatch, debian/rules,
    debian/install:  Building against scim 1.4, change module installation
    directory from 1.0.0 to 1.4.0.  (Closes: #324268, #324808)  [minghua]
  * debian/patches/{00list,01gcc-4.0.dpatch}:  Apply patch from Andreas Jochens
    <aj@andaco.de> to fix FTBFS with gcc-4.0.  (Closes: #297975)  [minghua]
  * debian/control, debian/{postinst,prerm}, debian/install:  This new release
    doesn't use gconf anymore, so get rid of gconf2 dependency and related
    scripts.  [minghua]
  * debian/{control,rules,install,docs}:  Build scim-chinese dummy package to
    help upgrade, rename install and docs to scim-pinyin.install and
    scim-pinyin.docs.  [minghua]
  * debian/control:  Upgrade to policy version 3.6.2 (no actual changes).
    [minghua]
  * debian/watch:  Track all new versions.  [minghua]
  * debian/copyright:  Update FSF address.  [minghua]

 -- Ming Hua <minghua@rice.edu>  Thu,  6 Oct 2005 21:28:53 -0500

scim-chinese (0.4.2-2) unstable; urgency=low

  * debian/{copyright,watch}:  Update upstream information.  [minghua]
  * debian/control:  [minghua]
    - Remove explicit dependency on scim since it's included in
      ${shlibs:Depends}.
    - Add scim (<< 1.1) to Depends: so that it won't be installed with scim
      1.2.x series.  This is better than setting Conflicts:.
    - Move Chinese TrueType font packages from Depends: to Recommends:.
  * debian/README.Debian:  New.  Basic information about scim-chinese and tips
    about ``pinyin with tone''.  [minghua]

 -- Ming Hua <minghua@rice.edu>  Sun,  6 Feb 2005 22:20:50 -0600

scim-chinese (0.4.2-1) unstable; urgency=low

  * New upstream release.
  * debian/control:  Update scim dependencies to >= 1.0.1, also updated
    package descriptions.
  * debian/rules, debian/patches/, debian/control:  Switch to dpatch system.
  * debian/watch:  Add watch file for monitoring upstream releases.
  * debian/postinst, debian/prerm:  Slight changes to postinst script
    according to dh_gconf(1) template, and add prerm script to unregister
    gconf schemas.

 -- Ming Hua <minghua@rice.edu>  Wed, 29 Sep 2004 17:38:06 -0500

scim-chinese (0.3.0-2) unstable; urgency=low

  * Initial release for upload into the official Debian archive.
  * Minor tweakings:
     - Added Build-Depends: libgtk2.0-dev
     - Added Depends: gconf2
     - Added postinst to run gconftool-2, based on upstream SPEC file.
  * NMU: Ming Hua <minghua@rice.edu> is still the maintainer.
    I am his Debian upload sponsor.  :-)

 -- Anthony Fok <foka@debian.org>  Sat, 12 Jun 2004 01:37:11 +0800

scim-chinese (0.3.0-1) unstable; urgency=low

  * Initial Release, based on upstream version 0.3.0.

 -- Ming Hua <minghua@rice.edu>  Fri, 30 Apr 2004 14:58:29 -0500

